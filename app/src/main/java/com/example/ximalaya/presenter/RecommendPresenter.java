package com.example.ximalaya.presenter;

import com.example.ximalaya.interfaces.IRecommendPresenter;
import com.example.ximalaya.interfaces.IRecommendViewCallBack;
import com.example.ximalaya.utils.Contants;
import com.example.ximalaya.utils.LogUtil;
import com.ximalaya.ting.android.opensdk.constants.DTransferConstants;
import com.ximalaya.ting.android.opensdk.datatrasfer.CommonRequest;
import com.ximalaya.ting.android.opensdk.datatrasfer.IDataCallBack;
import com.ximalaya.ting.android.opensdk.model.album.Album;
import com.ximalaya.ting.android.opensdk.model.album.GussLikeAlbumList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RecommendPresenter implements IRecommendPresenter {
    private static final String TAG = "RecommendPresenter";
    private List<IRecommendViewCallBack> mCallBacks = new ArrayList<>();
    private RecommendPresenter(){

    }
    private static RecommendPresenter sInstance = null;

    /**
     * 获取单例对象
     * @return
     */
    public static RecommendPresenter getInstance(){
        if(sInstance == null){
            synchronized (RecommendPresenter.class){
                if(sInstance == null){
                    sInstance = new RecommendPresenter();
                }
            }
        }
        return sInstance;
    }
    @Override
    public void getRecommendList() {
        //获取内容
        updateLoading();
        /**
         * 获取推荐内容 拿到 猜你喜欢 专辑
         */
        //封装数据
        Map<String, String> map = new HashMap<>();
        //这个参数表示一页书返回多少条
        map.put(DTransferConstants.LIKE_COUNT, Contants.RECOMMEND_COUNT+"");

        CommonRequest.getGuessLikeAlbum(map, new IDataCallBack<GussLikeAlbumList>() {
            @Override
            public void onSuccess(GussLikeAlbumList gussLikeAlbumList) {
                if (gussLikeAlbumList!=null){
                    List<Album> albumList = gussLikeAlbumList.getAlbumList();
                    //得到数据以后，更新ui
                    //upRecommendUI(albumList);
                    handlerRecommendResult(albumList);
                }
            }

            @Override
            public void onError(int i, String s) {
                LogUtil.d(TAG,"error --->" + i);
                LogUtil.d(TAG,"errorMsg --->" + s);
                handlerError(); //发生错误
            }
        });
    }

    private void handlerError() {
        //更新ui
        if(mCallBacks!=null){
            for(IRecommendViewCallBack callBack : mCallBacks){
                callBack.onNetworkError();
            }
        }
    }

    private void handlerRecommendResult(List<Album> albumList) {
        //更新ui
        if(albumList!=null){
            if(albumList.size()==0){
                for(IRecommendViewCallBack callBack : mCallBacks){
                    callBack.onEmpty();
                }
            }else {
                for(IRecommendViewCallBack callBack : mCallBacks){
                    callBack.onRecommendListLoaded(albumList);
                }
            }
        }
    }

    private void updateLoading(){
        for(IRecommendViewCallBack callBack : mCallBacks){
            callBack.onLoading();
        }
    }
    @Override
    public void pull2RefreshMore() {

    }

    @Override
    public void loadMore() {

    }

    @Override
    public void registerViewCallBack(IRecommendViewCallBack callBack) {
        if(mCallBacks!=null && !mCallBacks.contains(callBack)){
            mCallBacks.add(callBack);
        }
    }

    @Override
    public void unRegisterViewCallBack(IRecommendViewCallBack callBack) {
        if(mCallBacks!=null){
            mCallBacks.remove(mCallBacks);
        }
    }
}
